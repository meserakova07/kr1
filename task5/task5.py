import time
import methods
import get_slay as g_s
import graph as gh


def cramer_perebor():
    nxn = []
    elaps_time = []
    #--начинаем вычислять СЛАУ различной размерности
    for i in range(5, 12, 1):
        nxn.append(i)
        #--создание матрицы для решения СЛАУ
        A = g_s.gen_matrix_a(i)
        B = g_s.gen_matrix_b(i)

        #--запускаем секундомер
        start_time = time.time() 
        methods.cramer(A, B)
        #--останавливаем секундомер и вычисляем затраченное время
        stop_time = time.time()
        elaps_time.append(stop_time - start_time)
    #--возвращаем размерности СЛАУ и время решений
    return nxn, elaps_time


def gauss_perebor():
    nxn = []
    elaps_time = []
    #--начинаем вычислять СЛАУ различной размерности
    for i in range(100, 320, 20):
        nxn.append(i)
        #--создание матрицы для решения СЛАУ
        A = g_s.gen_matrix_a(i)
        B = g_s.gen_matrix_b(i)

        #--запускаем секундомер
        start_time = time.time() 
        methods.gauss(A, B)
        #--останавливаем секундомер и вычисляем затраченное время
        stop_time = time.time()
        elaps_time.append(stop_time - start_time)
    #--возвращаем размерности СЛАУ и время решений
    return nxn, elaps_time

#--получаем точки для графика Cramer
nxn, elaps_time = cramer_perebor()
#--создаём график Cramer
gh.graph(nxn, elaps_time, 'Крамера')

#--получаем точки для графика Gauss
nxn, elaps_time = gauss_perebor()
#--создаём график Gauss
gh.graph(nxn, elaps_time, 'Гаусса')