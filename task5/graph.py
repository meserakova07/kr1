import matplotlib.pyplot as plt


def graph(x, y, m):
    '''Строит график по заданным точкам'''
    plt.title(f'график зависимости времени решения системы методом {m} от её размерности')
    plt.xlabel('Размерность')
    plt.ylabel('Время') 
    plt.grid()   
    plt.plot(x, y)
    plt.show()