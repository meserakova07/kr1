import random

def gen_matrix_a(n):
    '''Генерирует матрицу 'А' СЛАУ заданной размерности'''
    a = [[random.randint(0, 10) for j in range(n)] for i in range(n)]
    return a


def gen_matrix_b(n):
    '''Генерирует матрицу 'В' СЛАУ заданной размерности'''
    b = [random.randint(0, 10) for j in range(n)] 
    return b