s = 42
#--получаем иксы и вычисляем сумму
while True:
    try:
        x = int(input())
        s += 1 / x
    except ValueError:
        continue
    except EOFError:
        break
    
#--выводим результат
print(s)