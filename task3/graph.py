import matplotlib.pyplot as plt


def data_for_graph(z_int, z_float):
    '''преобразует данные для построения графиков'''
    gh_x_int = []
    gh_y_int = []
    gh_x_float = []
    gh_y_float = []
    for i in range(len(z_int)):
        gh_x_int.append(z_int[i][0])
        gh_y_int.append(z_int[i][1])
        gh_x_float.append(z_float[i][0])
        gh_y_float.append(z_float[i][1])
    return gh_x_int, gh_y_int, gh_x_float, gh_y_float


def graph(gh_x_int, gh_y_int, gh_x_float, gh_y_float):
    '''Строит график по заданным точкам'''
    plt.title('Линейная зависимость времени вычисления вектора z от его длины')
    plt.plot(gh_x_int, gh_y_int, marker='o', label = 'int')
    plt.plot(gh_x_float, gh_y_float, marker='o', label = 'float')
    plt.xlabel('Длина')
    plt.ylabel('Время')
    plt.grid()
    plt.show()