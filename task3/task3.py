import time
import graph as gh
import saxpy
import vector as v


def saxpy_perebor():
    z_int = []
    z_float = []
    a = 17
    for n in range(1000000, 11000000, 1000000):
        #--генерируем два целочисленных вектора
        x_int, y_int = v.gen_int_vectors(n)
        #--вычисляем целочисленный saxpy и засекаем время
        start_time = time.time()
        z = saxpy.saxpy(a, x_int, y_int)
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        z_int.append([len(z), elapsed_time])

        #--генерируем два вещественных вектора
        x_float, y_float = v.gen_float_vectors(n)
        #--вычисляем вещественный saxpy и засекаем время
        start_time = time.time()
        z = saxpy.saxpy(a, x_float, y_float)
        stop_time = time.time()
        elapsed_time = stop_time - start_time
        z_float.append([len(z), elapsed_time])
    return z_int, z_float


#--вычисляем saxpy
z_int, z_float = saxpy_perebor()
#--создаём график
gh_x_int, gh_y_int, gh_x_float, gh_y_float = gh.data_for_graph(z_int, z_float)
gh.graph(gh_x_int, gh_y_int, gh_x_float, gh_y_float)