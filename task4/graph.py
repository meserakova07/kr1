import matplotlib.pyplot as plt


def data_for_graph():
    '''Подготавливает полученные данные для построения графика'''
    m_int, m_float = calc_matrix()
    m_int_n = []
    m_int_t = []
    m_float_n = []
    m_float_t = []
    for i in range(len(m_int)):
        m_int_n.append(m_int[i][0])
        m_int_t.append(m_int[i][1])
        m_float_n.append(m_float[i][0])
        m_float_t.append(m_float[i][1])
    return m_int_n, m_int_t, m_float_n, m_float_t

def graph(m_int_n, m_int_t, m_float_n, m_float_t):
    '''Строит график по заданным точкам'''
    plt.title('График зависимости времени подсчета от размерности матрицы')
    plt.plot(m_int_n, m_int_t, marker='o', label = 'int')
    plt.plot(m_float_n, m_float_t, marker='o', label = 'float')
    plt.xlabel('Размерность')
    plt.ylabel('Время')
    plt.grid() 
    plt.show()