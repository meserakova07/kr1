import time


frames = []
#--переключатель добавления строки
b = False
while True:
    #--получаем строку
    try:
        line = input()
    except EOFError:
        #--если строки кончились, то останавливаем цикл
        break
    
    #--если строка является границей кадра, то щёлкаем переключатель
    if line == '```':
        b = not b
        #--если граница кадра является начальной, то добавляем массив для хранения этого кадра
        if b == True:
            frames.append([])
        continue
    
    #--если переключатель "вкл", то добавлявляем строку в кадр
    if b == True:
        frames[-1].append(line)
    

#--выводим кадры
while True:
    for i in range(len(frames)):
        #--очищаём консоль
        print('\033c')
        for j in range(len(frames[i])):
            #--выводим строку кадра
            print(f'\033[96m{frames[i][j]}\033[0m')
        #--задерживаем стирание кадра
        time.sleep(0.6)
      
